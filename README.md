# Samply.Common.MDRFaces

Samply.Common.MDRFaces is a Java Server Faces library which eases the user
interface design of web applications where the respective data model relies on
well defined data elements. Especially in case of systems for electronic data
capturing, where the necessary data model is not known beforehand and can even
vary over time, the user interface has to be easily adjustable. This often means
the user instead of the developer designs the various forms for data entry and
therefore an easy to use mechanism has to be provided. By using Samply.MDRFaces
the developer can focus on the implementation of that mechanism, e.g. some
editor component, but does not have to cope with the visualization of every
single data element as for that is taken care of automatically.

# Features

- reuse predefined widgets which already contain a label and the appropriate
  input field
- the input field depends on the datatype of the underlying data element, e.g.
  Boolean (Checkbox), Date (Calendar), String (Text Input) or a List (Dropdown
Menu)
- uses Samply.MDRClient for the metadata retrieval from Samply.MDR
- validates the users input according to the validation information which is
  included in the metadata of a data element

# Build

In order to build this project, you need to configure maven properly.  See
[Samply.Maven](https://bitbucket.org/medinfo_mainz/samply.maven) for more
information.

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>mdrfaces</artifactId>
    <version>${version}</version>
</dependency>
```

# Usage

### Simple Dataelements
In order to render any widget for a dataelement, the MdrContext has to be initialized with an MdrClient
that uses the MDR you want to use. Then, include the namespace in the application you like to use
MDRFaces in, e.g. as `xmlns:mf="http://xmlns.jcp.org/jsf/composite/mdrfaces"` and simply supply the 
dataelement urn as mdrid to the element. E.g.
 `<mf:formMdrField mdrId="urn:mdr:dataelement:1:1" includeInput="true" includeLabel="true" />`

### Catalogues

Catalogues can be used in different ways. Either instantly loading the full catalogue, which does
neither require a newer MDR.rest api nor an additional REST api provided by the application itself or
dynamically loading them.

#### Instantly loaded
For legacy support, it is possible to directly load the full catalogue into either the HTML dom or
into memory. This may be ok for smaller catalogues, but will become very unsatisfying with larger
catalogues. This can be used like any other dataelement by simply providing the dataelement urn.

To load into the HTML dom, simply use:
`<mf:formMdrField mdrId="urn:mdr:dataelement:1:1" useDynamicCatalogues="false" ajaxCatalogues="false"/>`

Loading into memory requires the same endpoint as the dynamically loaded ones below (the xtml page).:
`<mf:formMdrField mdrId="urn:mdr:dataelement:1:1" useDynamicCatalogues="false"/>`

#### Dynamically loaded (preferred)
Using dynamically loaded catalogues requires an endpoint to get the catalogues themselves and 
modify them to fulfill the requirements of the catalogue script. This itself requires an MDR with REST
interface that supports that functionality (MDR.rest 4.1.0 or higher).

The endpoint can be implemented by adding a facelet to the calling application. See the example below.

It is also required to add a single catalogue modal in the application that uses MdrFaces. MdrFaces
will then set the content of the modal according to which element was clicked.

##### Example:

The following Facelet has to be added to the calling application.

```xhtml
<?xml version='1.0' encoding='UTF-8' ?>

<metadata xmlns="http://xmlns.jcp.org/jsf/core">
    <event type="preRenderView" listener="#{CatalogueSource.getCatalogueNode}"/>
</metadata>
```

By default, mdrfaces expects it to be accessible at `/path/to/your/application/catalogue_source.xhtml`.
If you want to place it at another location, that location has to be submitted to MdrContext during
initialization - `MdrContext.getMdrContext().init(mdrClient, "some_other_location.xhtml");`. By default,
this will prepend the context path before the source. If needed - you can disable this by using the
3-argument init method `MdrContext.getMdrContext().init(mdrClient, "some_other_location.xhtml", false);`
 
Next up, there needs to be **one** `dynamicCatalogueModal` on the page that uses the catalogues.
Content will be loaded via ajax upon clicking the button next to the input field. The modal component
requires no parameters, a simple `<mf:dynamicCatalogueModal />` is sufficient (assuming mdrfaces was
included with the prefix `mf` `xmlns:mf="http://xmlns.jcp.org/jsf/composite/mdrfaces"). Please note 
that the modal has to be inside a form.
 
Finally, use the `formMdrField` as usual, but set `useDynamicCatalogues` to `true`. E.g. 
`<mf:formMdrField mdrId="urn:mdr:dataelement:1:1" useDynamicCatalogues="true"/>`

##### Notes
Please note that it is _possible_ to set `useDynamicCatalogues` to `true` and `ajaxCatalouges` to `false`
(or the `ajax` parameter of a modal) - however it will be ignored since it would not make any sense.