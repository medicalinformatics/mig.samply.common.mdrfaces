# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.1.2] - 2022-07-07
### Fixed
- Record labels showed designation of first record member.

## [5.1.1] - 2021-06-01
### Fixed
- CSS class 'isRequired' was missing for mandatory text and textarea elements.

## [5.1.0] - 2021-05-17
### Added
- MDR data elements of type 'text' can now be rendered as textarea HTML tags. Data elements need a slot `renderType` with value `TEXTAREA`.
- Items of permitted values data elements can optionally be sorted by their value. Sorting occurs numerically if all values can be casted as integer numbers and alphabetically otherwise.
- Labels in datepickers are now shown in the selected language (as inferred from the JSF context).
### Changed
- Dynamic catalogues will only be used if the MDR supports them. Static catalogues will be used otherwise.
- The empty value is inserted at the beginning of the list of permitted values instead of at the end.
- Updated mdr client version

### Fixed
- add lodash and font-awesome dependencies
- Fixed invalid date because of the German language.

## [5.0.0] - 2020-05-26
### Added
- Commas entered as decimal separators in float values are automatically converted to points.
### Changed
- apply google java code style
### Fixed
- Number validation did not work for dataelement with only one limit (min/max) or with floating point numbers as limits.

## [4.2.0] - 2019-09-26
### Added
- Slots are rendered as attributes of the respective input element.
Naming convention: "data-slot-{slot name}". Slot names and values are sanitized to meet the 
restrictions on attribute names and values as documented in 
https://html.spec.whatwg.org/multipage/syntax.html#attributes-2. 
Note: rendering slots currently does not work when included in partial updates.
### Fixed
- new mdr client with working proxy config is used

## [4.1.0] - 2019-08-07
### Added
- Search/filter function available for dynamically loaded catalogs

### Changed
- Dynamically loaded catalogs are now all shown in the same modal

## [4.0.0] - 2019-06-19
### Changed
- Catalogs are now loaded lazily

### Removed
- Preloading catalogs is no longer possible

## [3.1.0] - 2019-07-18
### Added
- Fields and labels set as required now have the css class "isRequired".
- Input fields that have a unit of measure set in the MDR are now displayed as an input group with an addon showing the unit.
- Parameter to choose if the "isValid" attribute of a catalog node should mean that it is not
selectable

## [3.0.0] - 2018-12-19
### Added
- An attribute to skip the creation of a catalogue modal can be set. In this case, the calling application has to create
the modal itself. This is encouraged for any repeatable elements (plain dataelements or records alike) because it prevents
creating modals for EACH entry in the repeatable.

### Changed
- Updated mdr client version, including switch to jersey 2.x
- If a dataelement with the given urn is not found, display the urn instead of throwing an exception
- Use a searchable tree instead of select2 for catalogues

### Removed
- Unused clockpicker plugin
- Reverted the change from 2.2.0 that displayed the urn. Throw the exception again and have the client deal with this. 


## [2.1.1] - 2017-10-04
### Added
- Input elements now contain a ```data-mdrid``` attribute, which contains the URN of the data element. 

## [2.1.0] - 2017-08-29
### Added
- Records are now handled dynamically and labeled in the right language.

## [2.0.0] - 2017-07-25
