/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.test;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.junit.Test;

import de.samply.web.enums.EnumDateFormat;
import de.samply.web.enums.EnumTimeFormat;
import de.samply.web.mdrfaces.validators.DateTimeValidator;
import de.samply.web.mdrfaces.validators.DateValidator;
import de.samply.web.mdrfaces.validators.TimeValidator;

/**
 * Test class for data type validations. 
 * 
 * @author diogo
 *
 */
public class ValidationTest {

    /**
     * Test time formats from the {@link TimeValidator}.
     */
    @Test
    public final void testTimeFormats() {
        DateFormat formatter = new SimpleDateFormat(TimeValidator.getTimePattern(EnumTimeFormat.HOURS_24));
        String pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("HH:mm", pattern);

        formatter = new SimpleDateFormat(TimeValidator.getTimePattern(EnumTimeFormat.HOURS_24_WITH_SECONDS));
        pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("HH:mm:ss", pattern);

        formatter = new SimpleDateFormat(TimeValidator.getTimePattern(EnumTimeFormat.HOURS_12));
        pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("h:mm a", pattern);

        formatter = new SimpleDateFormat(TimeValidator.getTimePattern(EnumTimeFormat.HOURS_12_WITH_SECONDS));
        pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("h:mm:ss a", pattern);

        formatter = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.GERMANY);
        pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("HH:mm", pattern);

        formatter = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.GERMANY);
        pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("HH:mm:ss", pattern);
    }

    /**
     * Test time formats from the {@link DateValidator}.
     */
    @Test
    public final void testDateFormats() {
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.GERMANY);
        String pattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("dd.MM.yyyy", pattern);

        String localPattern = ((SimpleDateFormat) formatter).toPattern();
        assertEquals("dd.MM.yyyy", localPattern);

        String datepickerPattern = DateValidator.getDatePattern(EnumDateFormat.ISO_8601_WITH_DAYS);
        assertEquals("yyyy-MM-dd", datepickerPattern);

        formatter = SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM, Locale.GERMANY);
        assertEquals("dd.MM.yyyy", ((SimpleDateFormat) formatter).toPattern());
    }

    /**
     * Test time formats from the {@link DateValidator}.
     */
    @Test
    public final void testDateTimeFormats() {
        String pattern = DateTimeValidator.getDateTimePattern(EnumDateFormat.DIN_5008, EnumTimeFormat.HOURS_12);
        assertEquals("MM.yyyy h:mm a", pattern);

        pattern = DateTimeValidator.getDateTimePattern(EnumDateFormat.DIN_5008_WITH_DAYS,
                EnumTimeFormat.HOURS_12_WITH_SECONDS);
        assertEquals("dd.MM.yyyy h:mm:ss a", pattern);

        pattern = DateTimeValidator.getDateTimePattern(EnumDateFormat.ISO_8601_WITH_DAYS, EnumTimeFormat.HOURS_24);
        assertEquals("yyyy-MM-dd HH:mm", pattern);

        pattern = DateTimeValidator.getDateTimePattern(EnumDateFormat.LOCAL_DATE_WITH_DAYS, EnumTimeFormat.LOCAL_TIME);
        assertEquals("dd.MM.yyyy HH:mm", pattern);
    }

}
