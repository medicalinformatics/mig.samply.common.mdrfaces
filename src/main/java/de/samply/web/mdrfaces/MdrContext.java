/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces;

import de.samply.common.mdrclient.MdrClient;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Context of the MDR connection. This instance needs to be initialized before MdrFaces is used.
 *
 * @author diogo
 */
public enum MdrContext {
  /** Only instance of the MDR Context. */
  INSTANCE;

  public static final String DEFAULT_CATALOGUE_SOURCE = "/catalogue_source.xhtml";

  /** The MDR client instance. */
  private MdrClient mdrClient;

  /** The base URL to the resource where catalogues are accessible. */
  private String catalogueSource;

  private Logger logger = LoggerFactory.getLogger(MdrContext.class);

  /**
   * Get the MDR context.
   *
   * @return the MDR context
   */
  public static MdrContext getMdrContext() {
    return INSTANCE;
  }

  /**
   * Initialize MdrFaces with the desired MdrClient instance.
   *
   * @param mdrClient MDR client instance
   */
  public void init(final MdrClient mdrClient) {
    this.mdrClient = mdrClient;
    this.catalogueSource =
        ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                .getContextPath()
            + DEFAULT_CATALOGUE_SOURCE;
  }

  /**
   * Initialize MdrFaces with the desired MdrClient instance and rest base url
   *
   * <p>This will prepend the catalogue source with the context path.
   *
   * @param mdrClient MDR client instance
   * @param catalogueSource path to the facelet that includes the call to CatalogueSource
   */
  public void init(final MdrClient mdrClient, final String catalogueSource) {
    init(mdrClient, catalogueSource, true);
  }

  /**
   * Initialize MdrFaces with the desired MdrClient instance and rest base url.
   *
   * @param mdrClient MDR client instance
   * @param catalogueSource path to the facelet that includes the call to CatalogueSource
   * @param prependContextPath if set to true, the context path will be prepended to the
   *     catalogueSource. This path does not end with a slash, so make sure to have the catalogue
   *     source starting with one
   */
  public void init(
      final MdrClient mdrClient, final String catalogueSource, final boolean prependContextPath) {
    this.mdrClient = mdrClient;
    if (prependContextPath) {
      this.catalogueSource =
          ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                  .getContextPath()
              + catalogueSource;
    } else {
      this.catalogueSource = catalogueSource;
    }
  }

  /**
   * Get the mdrClient instance used to initialize the MDR context.
   *
   * @return the mdrClient instance used to initialize the MDR context.
   * @throws IllegalStateException if the MDR context has not been initialized.
   */
  public MdrClient getMdrClient() throws IllegalStateException {
    if (mdrClient != null) {
      return mdrClient;
    } else {
      throw new IllegalStateException(
          "The MDR context has not been initialised before MdrFaces usage.");
    }
  }

  public String getCatalogueSource() {
    return catalogueSource;
  }
}
