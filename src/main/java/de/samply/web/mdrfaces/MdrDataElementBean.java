/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces;

import com.google.common.base.CharMatcher;
import com.google.gson.Gson;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Catalogue;
import de.samply.common.mdrclient.domain.Code;
import de.samply.common.mdrclient.domain.Definition;
import de.samply.common.mdrclient.domain.Designation;
import de.samply.common.mdrclient.domain.EnumDataType;
import de.samply.common.mdrclient.domain.Identification;
import de.samply.common.mdrclient.domain.Meaning;
import de.samply.common.mdrclient.domain.PermissibleValue;
import de.samply.common.mdrclient.domain.RecordDefinition;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.Root;
import de.samply.common.mdrclient.domain.Slot;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.jsf.JsfUtils;
import de.samply.web.enums.EnumInputType;
import de.samply.web.enums.EnumRenderType;
import de.samply.web.mdrfaces.model.CatalogueNode;
import de.samply.web.mdrfaces.model.DynamicCatalogueNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.faces.bean.ManagedBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MDR data element bean that helps defining widgets from MDR data.
 *
 * @author diogo
 */
@ManagedBean(name = "MdrDataElementBean")
public class MdrDataElementBean {

  /**
   * Name of the JSF MDR ID attribute.
   */
  private static final String MDR_ID_JSF_ATTRIBUTE = "mdrId";

  /**
   * Name of the INPUT TYPE Slot.
   */
  private static final String SLOT_INPUT_TYPE = "inputType";

  /**
   * Name of the RENDER TYPE Slot.
   */
  private static final String SLOT_RENDER_TYPE = "renderType";

  /**
   * Logging instance for this class.
   */
  private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  /**
   * MDR client instance.
   */
  private MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

  /**
   * Automatically generated ID of the text input.
   */
  private String genInputTextId;
  /**
   * Automatically generated ID of the text input.
   */
  private String genInputTextAreaId;
  /**
   * Automatically generated ID of the date input.
   */
  private String genInputDateId;
  /**
   * Automatically generated ID of the date and time input.
   */
  private String genInputDateTimeId;
  /**
   * Automatically generated ID of the select one (drop-down box) input.
   */
  private String genInputSelectOneId;
  /**
   * Automatically generated ID of the boolean input.
   */
  private String genInputBooleanId;
  /**
   * Automatically generated ID of the time input.
   */
  private String genInputTimeId;
  /**
   * Automatically generated ID of the catalogue input.
   */
  private String genInputCatalogueId;

  /**
   * TODO: add javadoc.
   */
  public MdrDataElementBean() {
    int index = 0;

    genInputTextId = "input_" + index++;
    genInputTextAreaId = "input_" + index++;
    genInputDateId = "input_" + index++;
    genInputDateTimeId = "input_" + index++;
    genInputSelectOneId = "input_" + index++;
    genInputBooleanId = "input_" + index++;
    genInputTimeId = "input_" + index++;
    genInputCatalogueId = "input_" + index++;
  }

  /**
   * TODO: add javadoc.
   */
  public static final List<String> getRecordMembers(String recordUrn) {
    List<String> memberUrns = new ArrayList<>();
    try {
      MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
      // The language Code doesn't matter here, since only the urns are needed, so just get the
      // english ones
      List<Result> recordMembers = mdrClient.getRecordMembers(recordUrn, "en");

      for (Result member : recordMembers) {
        memberUrns.add(member.getId());
      }

    } catch (ExecutionException | MdrConnectionException e) {
      e.printStackTrace();
    }

    return memberUrns;
  }

  /**
   * Check if the mdr can find the mdr id.
   *
   * @param mdrId the urn of the dataelement to check for
   * @return true if it is found, false on any error
   */
  public final boolean isElementKnown(final String mdrId) {
    String path = "";
    if (mdrId.toLowerCase().contains(":dataelement:")) {
      path = "dataelements/" + mdrId;
    } else if (mdrId.toLowerCase().contains(":record:")) {
      path = "records/" + mdrId;
    } else {
      // Default to true
      return true;
    }
    return mdrClient.isObjectReadable(path, JsfUtils.getAccessToken());
  }

  /**
   * Get the text of the label of an MDR element in a form.
   *
   * @param mdrId the MDR element ID e.g. "urn:mdr:dataelement:2:1.0"
   * @return the text label of the requested data element
   */
  public final String getLabelText(final String mdrId) {
    String labelText = "";

    // Use MdrClient Methods for records if record is detected
    if (mdrId.toLowerCase().contains("record")) {

      try {
        RecordDefinition recordDefinition =
            mdrClient.getRecordDefinition(
                mdrId,
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        if (recordDefinition != null && recordDefinition.size() > 0) {
          labelText = recordDefinition.get(0).getDesignation().trim();
        }

        return labelText;
      } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
        return "?" + mdrId + "?";
      }
    } else {
      try {
        Definition dataElementDefinition =
            mdrClient.getDataElementDefinition(
                mdrId,
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        if (dataElementDefinition != null && dataElementDefinition.getDesignations().size() > 0) {
          labelText = dataElementDefinition.getDesignations().get(0).getDesignation().trim();
        }

        return labelText;
      } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
        return "?" + mdrId + "?";
      }
    }
  }

  /**
   * Get the language code of the currently selected language.
   *
   * @return the language code
   * @see JsfUtils#getLocaleLanguage()
   */
  public final String getLanguageCode() {
    return JsfUtils.getLocaleLanguage();
  }

  /**
   * Which input type shall be rendered for a given urn.
   *
   * @param mdrId the MDR data element loaded.
   * @return type to render
   */
  public final EnumInputType renderedInputType(final String mdrId) {
    Validations dataElementValidations = null;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input text will not be rendered: " + e.getMessage());
    }

    if (hasPermissibleValues(dataElementValidations)) {
      return EnumInputType.SELECTONE;
    } else if (dataElementValidations.getDatatype().equals(EnumDataType.DATE.name())) {
      return EnumInputType.DATE;
    } else if (dataElementValidations.getDatatype().equals(EnumDataType.DATETIME.name())) {
      return EnumInputType.DATETIME;
    } else if (dataElementValidations.getDatatype().equals(EnumDataType.BOOLEAN.name())) {
      return EnumInputType.BOOLEAN;
    } else if (dataElementValidations.getDatatype().equals(EnumDataType.TIME.name())) {
      return EnumInputType.TIME;
    } else if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
      return EnumInputType.CATALOGUE;
    } else {
      // Default to text input field
      return EnumInputType.TEXT;
    }
  }

  /**
   * Get the unit of measure of an MDR element in a form.
   *
   * @param mdrId the MDR element ID e.g. "urn:mdr:dataelement:2:1.0"
   * @return the unit of measure of a data element
   */
  public final String getUnitOfMeasure(final String mdrId) {
    String unitOfMeasure = "";

    if (mdrId != null && !mdrId.isEmpty()) {
      try {
        Definition dataElementDefinition =
            mdrClient.getDataElementDefinition(
                mdrId,
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        if (dataElementDefinition != null && dataElementDefinition.getDesignations().size() > 0) {
          unitOfMeasure = dataElementDefinition.getValidation().getUnitOfMeasure();
        }
      } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
        logger.warn("Could not read unit of measure. Returning empty string.");
      }
    }

    logger.debug("Unit of measure for " + mdrId + ": " + unitOfMeasure);

    return unitOfMeasure;
  }

  /**
   * Get the slots of an MDR element in a form.
   *
   * @param mdrId the MDR data element loaded.
   * @return The list of slots (empty list if no slots are defined for the element)
   */
  public final List<Slot> getSlots(final String mdrId) {
    try {
      return mdrClient.getDataElementSlots(
          mdrId, JsfUtils.getAccessToken(), JsfUtils.getUserAuthId());
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.warn("Could not read slots. Returning empty list.", e);
      return new ArrayList<>(0);
    }
  }

  /**
   * Whether the text input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if the text input should be rendered, false otherwise
   */
  public final boolean isInputTextRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return false;
      }

      String datatype = dataElementValidations.getDatatype();
      return datatype.compareTo(EnumDataType.INTEGER.name()) == 0
          || datatype.compareTo(EnumDataType.FLOAT.name()) == 0
          || datatype.compareTo(EnumDataType.STRING.name()) == 0;
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input text will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the date input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if the date input should be rendered, false otherwise
   */
  public final boolean isInputDateRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return false;
      }

      if (dataElementValidations.getDatatype().equals(EnumDataType.DATE.name())) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input date will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the date and time input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if the date and time input should be rendered, false otherwise
   */
  public final boolean isInputDateTimeRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return false;
      }

      if (dataElementValidations.getDatatype().equals(EnumDataType.DATETIME.name())) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input date will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the drop down box input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if the drop down box input should be rendered, false otherwise
   */
  public final boolean isInputSelectOneRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input select one will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the boolean input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if there is a boolean to be rendered, false otherwise
   */
  public final boolean isInputBooleanRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return false;
      }

      if (dataElementValidations.getDatatype().equals(EnumDataType.BOOLEAN.name())) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input boolean will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the time input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if a time widget is to be rendered, false otherwise
   */
  public final boolean isInputTimeRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (hasPermissibleValues(dataElementValidations)) {
        return false;
      }

      if (dataElementValidations.getDatatype().equals(EnumDataType.TIME.name())) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input time will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Whether the catalogue input should be rendered for the given data element or not.
   *
   * @param mdrId the MDR data element loaded.
   * @return true if the catalogue widget is to be rendered, false otherwise
   */
  public final boolean isInputCatalogueRendered(final String mdrId) {
    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input time will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Get the label tool tip text.
   *
   * @return the label tool tip text.
   */
  public final String getlabelTooltipText() {
    String mdrId = JsfUtils.getJsfAttribute(MDR_ID_JSF_ATTRIBUTE);
    try {
      Definition dataElementDefinition =
          mdrClient.getDataElementDefinition(
              mdrId,
              JsfUtils.getLocaleLanguage(),
              JsfUtils.getAccessToken(),
              JsfUtils.getUserAuthId());

      if (dataElementDefinition != null
          && dataElementDefinition.getDesignations() != null
          && !dataElementDefinition.getDesignations().isEmpty()) {
        return dataElementDefinition.getDesignations().get(0).getDefinition();
      }
      return null;
    } catch (ExecutionException | MdrInvalidResponseException | MdrConnectionException e) {
      return null;
    }
  }

  /**
   * Get the input tool tip text.
   *
   * @return the input tool tip text.
   */
  public final String getInputTooltipText() {
    String mdrId = JsfUtils.getJsfAttribute(MDR_ID_JSF_ATTRIBUTE);

    try {
      Definition dataElementDefinition =
          mdrClient.getDataElementDefinition(
              mdrId,
              JsfUtils.getLocaleLanguage(),
              JsfUtils.getAccessToken(),
              JsfUtils.getUserAuthId());

      if (dataElementDefinition != null
          && dataElementDefinition.getDesignations() != null
          && dataElementDefinition.getDesignations().size() > 0) {
        return dataElementDefinition.getDesignations().get(0).getDefinition();
      }
      return null;
    } catch (ExecutionException | MdrInvalidResponseException | MdrConnectionException e) {
      return null;
    }
  }

  /**
   * Whether the tooltip should be rendered or not.
   *
   * @return true if the tooltip should be rendered, false otherwise
   */
  public final boolean isInputTooltipRendered() {
    String text = null;
    String mdrId = JsfUtils.getJsfAttribute(MDR_ID_JSF_ATTRIBUTE);
    try {
      Definition dataElementDefinition =
          mdrClient.getDataElementDefinition(
              mdrId,
              JsfUtils.getLocaleLanguage(),
              JsfUtils.getAccessToken(),
              JsfUtils.getUserAuthId());

      if (dataElementDefinition != null
          && dataElementDefinition.getDesignations() != null
          && dataElementDefinition.getDesignations().size() > 0) {
        text = dataElementDefinition.getDesignations().get(0).getDefinition();
      }

      if (text != null && text.length() > 0) {
        return true;
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Input tooltip will not be rendered: " + e.getMessage());
    }
    return false;
  }

  /**
   * Get the select items from an enumerated MDR element (the kind that is shown on a drop-down
   * box).
   *
   * @param mdrId of the MDR element
   * @return the select items of a MDR element
   */
  public final ArrayList<PermissibleValue> getSelectItems(final String mdrId) {
    return getSelectItems(mdrId, true, false, null);
  }

  /**
   * Get the select items from an enumerated MDR element (the kind that is shown on a drop-down
   * box).
   *
   * @param mdrId of the MDR element
   * @param includeEmptyElement Whether to include an empty element that represents no value.
   * @return the select items of a MDR element
   */
  public final ArrayList<PermissibleValue> getSelectItems(
      final String mdrId, final boolean includeEmptyElement) {
    return getSelectItems(mdrId, includeEmptyElement, false, null);
  }

  /**
   * Get the select items from an enumerated MDR element (the kind that is shown on a drop-down
   * box).
   *
   * @param mdrId The ID of the MDR element.
   * @param includeEmptyElement Whether to include an empty element that represents no value.
   * @param useSortOrder Whether to sort the single permitted values alphabetically.
   * @param option of the MDR element for example: changeable order of permitted values.
   * @return The select items of the MDR element.
   */
  public final ArrayList<PermissibleValue> getSelectItems(
      final String mdrId, final boolean includeEmptyElement, final boolean useSortOrder,
      final String option) {
    ArrayList<PermissibleValue> permissibleValues = new ArrayList<>();

    Validations dataElementValidations;
    try {
      dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();
      if (hasPermissibleValues(dataElementValidations)) {
        logger.debug("There is a list of permissible values for mdrId: " + mdrId);

        //Order of permitted values is the same as order from form.
        if (!useSortOrder && option != null && !option.isEmpty()) {
          String [] numbers = option.split(",");
          int size = dataElementValidations.getPermissibleValues().size();
          if (numbers.length == size) {
            for (int index = 0; index < size; index++) {
              try {
                Integer number = Integer.valueOf(numbers[index]);
                permissibleValues.add(dataElementValidations.getPermissibleValues().get(number));
              } catch (NumberFormatException | IndexOutOfBoundsException e) {
                permissibleValues.addAll(dataElementValidations.getPermissibleValues());
              }
            }
          } else {
            permissibleValues.addAll(dataElementValidations.getPermissibleValues());
          }
        } else {
          permissibleValues.addAll(dataElementValidations.getPermissibleValues());
        }

        if (useSortOrder) {
          // sort the values numerically if they are numbers
          try {
            Collections.sort(permissibleValues,
                (value1, value2) -> new Integer(value1.getValue())
                    .compareTo(new Integer(value2.getValue())));
          } catch (NumberFormatException e) {
            // otherwise, sort the values alphabetically
            Collections.sort(permissibleValues,
                (value1, value2) -> value1.getValue().compareToIgnoreCase(value2.getValue()));
          }
        }

        // enable an empty value (default selection)
        if (includeEmptyElement) {
          permissibleValues.add(0, getEmptyPermissableValue());
        }

      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug(e.getMessage());
    }
    return permissibleValues;
  }

  /**
   * TODO: add javadoc.
   */
  public final String getInputType(final String mdrId)
      throws MdrConnectionException, MdrInvalidResponseException, ExecutionException {
    EnumInputType inputType = EnumInputType.SELECT_ONE_MENU;
    String retVal = inputType.toString();

    ArrayList<Slot> slots = mdrClient.getDataElementSlots(mdrId);

    for (Slot slot : slots) {
      if (slot.getSlotName().equalsIgnoreCase(SLOT_INPUT_TYPE)) {
        try {
          retVal = EnumInputType.valueOf(slot.getSlotValue()).toString();
          break;
        } catch (IllegalArgumentException e) {
          logger.warn("Illegal Argument in inputType slot: " + slot.getSlotValue().toString());
          retVal = inputType.toString();
          break;
        }
      }
    }

    return retVal;
  }

  /**
   * Function to get specific rendertype if set in slot.
   */
  public final String getRenderType(final String mdrId)
      throws MdrConnectionException, MdrInvalidResponseException, ExecutionException {
    EnumRenderType renderType = EnumRenderType.INPUT_TYPE_TEXT;
    String retVal = renderType.toString();

    ArrayList<Slot> slots = mdrClient.getDataElementSlots(mdrId);

    for (Slot slot : slots) {
      if (slot.getSlotName().equalsIgnoreCase(SLOT_RENDER_TYPE)) {
        try {
          retVal = EnumRenderType.valueOf(slot.getSlotValue()).toString();
          break;
        } catch (IllegalArgumentException e) {
          logger.warn("Illegal Argument in renderType slot: " + slot.getSlotValue().toString());
          retVal = renderType.toString();
          break;
        }
      }
    }

    return retVal;
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId the MDR ID of the catalogue
   * @return the select item from a MDR catalogue
   */
  public final String getCatalogueJson(final String mdrId) {
    return getCatalogueJson(mdrId, null, true);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId         the MDR ID of the catalogue
   * @param selectedValue the currently selected value
   * @return the select item from a MDR catalogue
   */
  public final String getCatalogueJson(final String mdrId, final String selectedValue) {
    return getCatalogueJson(mdrId, selectedValue, true);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId                    the MDR ID of the catalogue
   * @param selectedValue            the currently selected value
   * @param onlyValidNodesSelectable whether or not the "isValid" attribute should be reflected when
   *                                 making a node selectable or not
   * @return the select item from a MDR catalogue
   */
  public final String getCatalogueJson(
      final String mdrId, final String selectedValue, final boolean onlyValidNodesSelectable) {
    List<DynamicCatalogueNode> catalogueNodes = new ArrayList<>();

    try {
      Validations dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        Catalogue catalogue =
            mdrClient.getDataElementCatalogue(
                mdrId,
                "root",
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        Root root = catalogue.getRoot();

        for (String subCode : root.getSubCodes()) {
          if (subCode.equalsIgnoreCase("null")) {
            continue;
          }
          DynamicCatalogueNode cn =
              DynamicCatalogueBean.createCatalogueNodeForCode(
                  catalogue, subCode, selectedValue, onlyValidNodesSelectable);
          catalogueNodes.add(cn);
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug(e.getMessage());
    }

    Collections.sort(catalogueNodes);
    return new Gson().toJson(catalogueNodes);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId the MDR ID of the catalogue
   * @return the select item from a MDR catalogue
   */
  public final String getFullCatalogueJson(final String mdrId) {
    return getFullCatalogueJson(mdrId, null, true);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId         the MDR ID of the catalogue
   * @param selectedValue the currently selected value
   * @return the select item from a MDR catalogue
   */
  public final String getFullCatalogueJson(final String mdrId, final String selectedValue) {
    return getFullCatalogueJson(mdrId, selectedValue, true);
  }

  //    private CatalogueNode createCatalogueNodeForCode(Catalogue catalogue, String code, String
  // selectedValue) {
  //        CatalogueNode catalogueNode = new CatalogueNode();
  //        Optional<Code> thisCodeOptional = Iterables.tryFind(catalogue.getCodes(), new
  // CodePredicate(code));
  //        if (thisCodeOptional.isPresent()) {
  //            Code thisCode = thisCodeOptional.get();
  //            List<Object> subCodes = thisCode.getSubCodes();
  //
  //            if (subCodes != null && subCodes.size() > 0) {
  //                catalogueNode.setChildren(new ArrayList<CatalogueNode>());
  //                for (Object subCode : subCodes) {
  //                    Optional<Code> subCodeOptional = Iterables.tryFind(catalogue.getCodes(), new
  // CodePredicate((String) subCode));
  //                    catalogueNode.getChildren().add(createCatalogueNodeForCode(catalogue,
  // subCodeOptional.get().getCode(), selectedValue));
  //                }
  //                if (catalogueNode.getChildren() != null) {
  //                    Collections.sort(catalogueNode.getChildren());
  //                }
  //            }
  //
  //            catalogueNode.setCode(code);
  //            String designation = MdrUtils.getDesignation(thisCode.getDesignations());
  //            String definition = MdrUtils.getDefinition(thisCode.getDesignations());
  //            StringBuilder nodeText = new StringBuilder();
  //            if (thisCode.getIsValid()) {
  //                nodeText.append("[");
  //                nodeText.append(code);
  //                nodeText.append("]: ");
  //            }
  //
  //            nodeText.append(designation);
  //            if (definition != null && !definition.equals(designation)) {
  //                nodeText.append(" (");
  //                nodeText.append(definition);
  //                nodeText.append(")");
  //            }
  //
  //            catalogueNode.setText(nodeText.toString());
  //            CatalogueItree itree = new CatalogueItree();
  //            State state = new State();
  //
  //            // if this is the selected code, set the corresponding state for the node
  ////            if (selectedValue != null && !selectedValue.isEmpty() &&
  // selectedValue.equals(code)) {
  ////                state.setSelected(true);
  ////            }
  //            state.setSelectable(thisCode.getIsValid());
  //            itree.setState(state);
  //            catalogueNode.setItree(itree);
  //        }
  //        return catalogueNode;
  //    }

  /**
   * Get the full (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId                    the MDR ID of the catalogue
   * @param selectedValue            the currently selected value
   * @param onlyValidNodesSelectable whether or not the "isValid" attribute should be reflected when
   *                                 making a node selectable or not
   * @return the select item from a MDR catalogue
   */
  public final String getFullCatalogueJson(
      final String mdrId, final String selectedValue, final boolean onlyValidNodesSelectable) {

    if (CatalogueBean.getCatalogues().containsKey(mdrId)) {
      return CatalogueBean.getCatalogues().get(mdrId);
    }

    List<CatalogueNode> catalogueNodes = new ArrayList<>();

    try {
      Validations dataElementValidations =
          mdrClient
              .getDataElementDefinition(
                  mdrId,
                  JsfUtils.getLocaleLanguage(),
                  JsfUtils.getAccessToken(),
                  JsfUtils.getUserAuthId())
              .getValidation();

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        Catalogue catalogue =
            mdrClient.getDataElementCatalogue(
                mdrId,
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        Root root = catalogue.getRoot();

        for (String subCode : root.getSubCodes()) {
          if (subCode.equalsIgnoreCase("null")) {
            continue;
          }
          CatalogueNode cn =
              CatalogueBean.createCatalogueNodeForCode(
                  catalogue, subCode, selectedValue, onlyValidNodesSelectable);
          catalogueNodes.add(cn);
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug(e.getMessage());
    }

    Collections.sort(catalogueNodes);

    String catalogueAsString = new Gson().toJson(catalogueNodes);
    CatalogueBean.getCatalogues().put(mdrId, catalogueAsString);

    return catalogueAsString;
  }

  /**
   * Get the ID of the input that will be rendered for the given MDR ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the input that will be rendered for the given MDR ID
   */
  public final String getRenderedInputId(final String mdrId, final String givenInputId) {
    String renderedInputId = "";
    if (!givenInputId.isEmpty()) {
      renderedInputId = givenInputId;
    } else {
      if (isInputTextRendered(mdrId)) {
        renderedInputId = genInputTextId;
      } else if (isInputDateRendered(mdrId)) {
        renderedInputId = genInputDateId;
      } else if (isInputDateTimeRendered(mdrId)) {
        renderedInputId = genInputDateTimeId;
      } else if (isInputSelectOneRendered(mdrId)) {
        renderedInputId = genInputSelectOneId;
      } else if (isInputBooleanRendered(mdrId)) {
        renderedInputId = genInputBooleanId;
      } else if (isInputTimeRendered(mdrId)) {
        renderedInputId = genInputTimeId;
      } else if (isInputCatalogueRendered(mdrId)) {
        renderedInputId = genInputCatalogueId;
      }
    }

    return renderedInputId;
  }

  /**
   * Get an empty permissible value to serve as an empty option.
   *
   * @return an empty permissible value to serve as an empty option
   */
  private PermissibleValue getEmptyPermissableValue() {
    PermissibleValue emptySelection = new PermissibleValue();
    ArrayList<Meaning> meanings = new ArrayList<Meaning>();
    Meaning meaning = new Meaning();
    meaning.setDesignation("");
    meanings.add(meaning);
    emptySelection.setMeanings(meanings);
    emptySelection.setValue("");
    return emptySelection;
  }

  /**
   * Get an empty catalogue code value to serve as an empty option.
   *
   * @return an empty catalogue code to serve as an empty option
   */
  private Code getEmptyCode() {
    Code code = new Code();
    code.setCode("");
    List<Designation> designations = new ArrayList<Designation>();
    Designation designation = new Designation();
    designation.setDefinition("");
    designation.setDesignation("");
    designations.add(designation);
    code.setDesignations(designations);
    Identification identification = new Identification();
    code.setIdentification(identification);
    code.setIsValid(true);
    List<Object> subCodes = new ArrayList<>();
    code.setSubCodes(subCodes);
    return code;
  }

  /**
   * Check if validations include permissible values.
   *
   * @param dataElementValidations the data element validations
   * @return true if {@link Validations} include permissible values, false otherwise
   */
  private boolean hasPermissibleValues(final Validations dataElementValidations) {
    return dataElementValidations == null
        || dataElementValidations.getPermissibleValues() != null
        && dataElementValidations.getPermissibleValues().size() > 0;
  }

  /**
   * Get the boolean input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the boolean input
   */
  public final String getInputBooleanId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputBooleanRendered(mdrId)
        ? genInputBooleanId
        : givenInputId;
  }

  /**
   * Get the date input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the date input
   */
  public final String getInputDateId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputDateRendered(mdrId) ? genInputDateId : givenInputId;
  }

  /**
   * Get the date and time input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the date and time input
   */
  public final String getInputDateTimeId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputDateTimeRendered(mdrId)
        ? genInputDateTimeId
        : givenInputId;
  }

  /**
   * Get the select one input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the select one input
   */
  public final String getInputSelectOneId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputSelectOneRendered(mdrId)
        ? genInputSelectOneId
        : givenInputId;
  }

  /**
   * Get the text input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the text input
   */
  public final String getInputTextId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputTextRendered(mdrId) ? genInputTextId : givenInputId;
  }

  /**
   * Get the text input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the text input
   */
  public final String getInputTextAreaId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputTextRendered(mdrId) ? genInputTextAreaId
        : givenInputId;
  }

  /**
   * Get the time input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the time input
   */
  public final String getInputTimeId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputTimeRendered(mdrId) ? genInputTimeId : givenInputId;
  }

  /**
   * Get the catalogue input ID.
   *
   * @param mdrId        the MDR ID of the rendered data element
   * @param givenInputId the input ID optionally given as a composite component attribute
   * @return the ID of the catalogue input
   */
  public final String getInputCatalogueId(final String mdrId, final String givenInputId) {
    return givenInputId.isEmpty() || !isInputCatalogueRendered(mdrId)
        ? genInputCatalogueId
        : givenInputId;
  }

  /**
   * Escape string so that it can safely be used as an HTML attribute name. See
   * https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
   *
   * @param name the string to escape
   * @return the escaped string
   */
  public final String escapeAttributeName(String name) {
    // Remove control characters
    String escapedName = CharMatcher.javaIsoControl().removeFrom(name);
    // Replace spaces by underscore
    escapedName = CharMatcher.whitespace().replaceFrom(escapedName, '_');
    // Remove other characters that are illegal in attribute names
    escapedName = escapedName.replaceAll("[\"'>/=]", "");
    // The linked document also states noncharacters as illegal, but
    // there is probably no way to get these into the MDR anyway.
    return escapedName;
  }

  /**
   * Escape string so that it can safely used as an HTML attribute value (quoted with double
   * quotes). See https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
   *
   * @param value the string to escape
   * @return the escaped string
   */
  public final String escapeAttributeValue(String value) {
    // In order to prevent ambiguous ampersands, escape all ampersands
    // (under the assumption that ampersands in slot values are meant to be
    // literal ampersands).
    String escapedValue = value.replaceAll("&", "&amp;");
    // Escape double quotes.
    escapedValue = value.replace("\"", "&quot;");
    return escapedValue;
  }
}
