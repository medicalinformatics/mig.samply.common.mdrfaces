/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces.validators;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.EnumValidationType;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.jsf.JsfUtils;
import de.samply.web.enums.EnumDateFormat;
import de.samply.web.mdrfaces.MdrContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator for date inputs.
 *
 * @author diogo
 */
@FacesValidator("DateValidator")
public class DateValidator implements Validator {

  /** Logging instance for this class. */
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  /** MDR client instance. */
  private MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

  /**
   * Get the date format pattern from the MDR date format description that the date/time picker can
   * understand.
   *
   * @param enumDateFormat the date format as known in MDR
   * @return the date format pattern string representation that the date/time picker can understand
   */
  public static String getDatepickerPattern(final EnumDateFormat enumDateFormat) {
    String datePattern = getDatePattern(enumDateFormat);
    return datePattern.toUpperCase();
  }

  /**
   * Get the date format pattern from the MDR date format description.
   *
   * @param enumDateFormat the date format as known in MDR
   * @return the date format pattern string representation
   */
  public static String getDatePattern(final EnumDateFormat enumDateFormat) {
    DateFormat formatter;
    switch (enumDateFormat) {
      case DIN_5008:
        formatter = new SimpleDateFormat("MM.yyyy");
        break;
      case DIN_5008_WITH_DAYS:
        formatter = new SimpleDateFormat("dd.MM.yyyy");
        break;
      case ISO_8601:
        formatter = new SimpleDateFormat("yyyy-MM");
        break;
      case ISO_8601_WITH_DAYS:
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        break;
      case LOCAL_DATE: // not valid
      case LOCAL_DATE_WITH_DAYS:
      default:
        formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, JsfUtils.getLocale());
        break;
    }
    return ((SimpleDateFormat) formatter).toPattern();
  }

  @Override
  public final void validate(
      final FacesContext context, final UIComponent component, final Object value)
      throws ValidatorException {
    String mdrId = (String) component.getAttributes().get("mdrId");

    try {
      Validations dataElementValidations =
          mdrClient.getDataElementValidations(
              mdrId,
              JsfUtils.getLocaleLanguage(),
              JsfUtils.getAccessToken(),
              JsfUtils.getUserAuthId());

      if (dataElementValidations.getValidationType().equals(EnumValidationType.DATE.name())) {

        // get the enum date format from the mdr data element
        EnumDateFormat enumDateFormat =
            EnumDateFormat.valueOf(dataElementValidations.getValidationData());
        // translate it to a pattern
        String datePattern = getDatePattern(enumDateFormat);
        SimpleDateFormat sdf;
        if (EnumDateFormat.LOCAL_DATE_WITH_DAYS.equals(enumDateFormat)) {
          //Datetimepicker.js supports English for month format "MMM" only.
          sdf = new SimpleDateFormat(datePattern, Locale.ENGLISH);
        } else {
          sdf = new SimpleDateFormat(datePattern);
        }
        sdf.setLenient(false);

        try {
          // if not valid, it will throw ParseException
          Date date = sdf.parse(value.toString());
          logger.debug("Correctly parsed date: " + date);
        } catch (ParseException e) {
          FacesMessage msg =
              new FacesMessage(
                  dataElementValidations.getErrorMessages().get(0).getDesignation(),
                  dataElementValidations.getErrorMessages().get(0).getDefinition());
          msg.setSeverity(FacesMessage.SEVERITY_ERROR);
          throw new ValidatorException(msg);
        }
      }

    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      logger.debug("Could not get the data elements validation for mdrId " + mdrId);
    } catch (NumberFormatException e) {
      logger.debug("Value is not a number... " + mdrId);
    }
  }
}
