/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces.validators;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.jsf.JsfUtils;
import de.samply.web.mdrfaces.MdrContext;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator for integers.
 *
 * @author diogo
 */
@FacesValidator("IntegerValidator")
public class IntegerValidator implements Validator {

  /** A integer regular expression. */
  private static final String DIGIT_ONLY_REGEX = "^[0-9]*$";

  /** Logging instance for this class. */
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  /** MDR client instance. */
  private MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

  /** The pattern of a integer regular expression. */
  private Pattern pattern;

  /** Create a new integer validator. */
  public IntegerValidator() {
    pattern = Pattern.compile(DIGIT_ONLY_REGEX);
  }

  @Override
  public final void validate(
      final FacesContext context, final UIComponent component, final Object value)
      throws ValidatorException {
    String mdrId = (String) component.getAttributes().get("mdrId");

    Matcher matcher = pattern.matcher(value.toString());
    if (!matcher.matches()) {

      Validations dataElementValidations;
      try {
        dataElementValidations =
            mdrClient.getDataElementValidations(
                mdrId,
                JsfUtils.getLocaleLanguage(),
                JsfUtils.getAccessToken(),
                JsfUtils.getUserAuthId());

        FacesMessage msg =
            new FacesMessage(
                dataElementValidations.getErrorMessages().get(0).getDesignation(),
                dataElementValidations.getErrorMessages().get(0).getDefinition());
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        throw new ValidatorException(msg);
      } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
        logger.debug("Could not get the data elements validation for mdrId " + mdrId);
      }
    }
  }
}
