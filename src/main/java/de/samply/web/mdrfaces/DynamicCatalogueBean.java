/*
 * Copyright (C) 2019 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces;

import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Catalogue;
import de.samply.common.mdrclient.domain.Code;
import de.samply.common.mdrclient.domain.CodePredicate;
import de.samply.common.mdrclient.domain.EnumDataType;
import de.samply.common.mdrclient.domain.Root;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.jsf.MdrUtils;
import de.samply.web.mdrfaces.model.CatalogueItree;
import de.samply.web.mdrfaces.model.DynamicCatalogueNode;
import de.samply.web.mdrfaces.model.State;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "DynamicCatalogueBean")
@ApplicationScoped
public class DynamicCatalogueBean implements Serializable {

  public static final String getCatalogueJson(
      final String mdrId,
      final String nodeCode,
      final String selectedValue,
      final String language,
      String accessToken,
      String authUserId) {
    return getCatalogueJson(mdrId, nodeCode, selectedValue, language, accessToken, authUserId);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId the MDR ID of the catalogue
   * @return the select item from a MDR catalogue
   */
  public static final String getCatalogueJson(
      final String mdrId,
      final String nodeCode,
      final String selectedValue,
      final String language,
      String accessToken,
      String authUserId,
      boolean onlyValidNodesSelectable) {

    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

    List<DynamicCatalogueNode> catalogueNodes = new ArrayList<>();

    try {
      Validations dataElementValidations =
          mdrClient.getDataElementValidations(mdrId, language, accessToken, authUserId);

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        Catalogue catalogue;
        if (nodeCode != null) {
          catalogue =
              mdrClient.getDataElementCatalogue(mdrId, nodeCode, language, accessToken, authUserId);
        } else {
          catalogue =
              mdrClient.getDataElementCatalogue(mdrId, "root", language, accessToken, authUserId);
        }

        Root root = catalogue.getRoot();

        for (String subCode : root.getSubCodes()) {
          if (subCode.equalsIgnoreCase("null")) {
            continue;
          }
          DynamicCatalogueNode cn =
              createCatalogueNodeForCode(
                  catalogue, subCode, selectedValue, onlyValidNodesSelectable);
          catalogueNodes.add(cn);
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      e.printStackTrace();
    }

    Collections.sort(catalogueNodes);

    return new Gson().toJson(catalogueNodes);
  }

  public static final String getCatalogueJson(
      final String mdrId,
      final String nodeCode,
      final String language,
      String accessToken,
      String authUserId) {
    return getCatalogueJson(mdrId, nodeCode, null, language, accessToken, authUserId);
  }

  /**
   * TODO: add javadoc.
   */
  public static DynamicCatalogueNode createCatalogueNodeForCode(
      Catalogue catalogue, String code, String selectedValue, boolean onlyValidNodesSelectable) {
    DynamicCatalogueNode catalogueNode = new DynamicCatalogueNode();
    com.google.common.base.Optional<Code> thisCodeOptional =
        Iterables.tryFind(catalogue.getCodes(), new CodePredicate(code));
    if (thisCodeOptional.isPresent()) {
      Code thisCode = thisCodeOptional.get();

      catalogueNode.setCode(code);
      catalogueNode.setId(thisCode.getIdentification().getUrn());
      String designation = MdrUtils.getDesignation(thisCode.getDesignations());
      String definition = MdrUtils.getDefinition(thisCode.getDesignations());
      StringBuilder nodeText = new StringBuilder();
      if (thisCode.getIsValid()) {
        nodeText.append("[");
        nodeText.append(code);
        nodeText.append("]: ");
      }

      nodeText.append(designation);
      if (definition != null && !definition.equals(designation)) {
        nodeText.append(" (");
        nodeText.append(definition);
        nodeText.append(")");
      }

      catalogueNode.setText(nodeText.toString());
      catalogueNode.setChildren(thisCode.getHasSubcodes());

      CatalogueItree itree = new CatalogueItree();
      State state = new State();

      // if this is the selected code, set the corresponding state for the node
      //            if (selectedValue != null && !selectedValue.isEmpty() &&
      // selectedValue.equals(code)) {
      //                state.setSelected(true);
      //            }
      state.setSelectable(!onlyValidNodesSelectable || thisCode.getIsValid());
      itree.setState(state);
      catalogueNode.setItree(itree);
    }
    return catalogueNode;
  }

  @PostConstruct
  public void init() {}
}
