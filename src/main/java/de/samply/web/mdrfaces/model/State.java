/*
 * Copyright (C) 2019 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces.model;

import com.google.gson.annotations.SerializedName;

public class State {
  private Boolean checked;
  private Boolean collapsed;
  private Boolean draggable;

  @SerializedName("drop-target")
  private Boolean dropTarget;

  private Boolean editable;
  private Boolean focused;
  private Boolean hidden;
  private Boolean indeterminate;
  private Boolean loading;
  private Boolean matched;
  private Boolean removed;
  private Boolean rendered;
  private Boolean selectable;
  private Boolean selected;

  public Boolean getChecked() {
    return checked;
  }

  public void setChecked(Boolean checked) {
    this.checked = checked;
  }

  public Boolean getCollapsed() {
    return collapsed;
  }

  public void setCollapsed(Boolean collapsed) {
    this.collapsed = collapsed;
  }

  public Boolean getDraggable() {
    return draggable;
  }

  public void setDraggable(Boolean draggable) {
    this.draggable = draggable;
  }

  public Boolean getDropTarget() {
    return dropTarget;
  }

  public void setDropTarget(Boolean dropTarget) {
    this.dropTarget = dropTarget;
  }

  public Boolean getEditable() {
    return editable;
  }

  public void setEditable(Boolean editable) {
    this.editable = editable;
  }

  public Boolean getFocused() {
    return focused;
  }

  public void setFocused(Boolean focused) {
    this.focused = focused;
  }

  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  public Boolean getIndeterminate() {
    return indeterminate;
  }

  public void setIndeterminate(Boolean indeterminate) {
    this.indeterminate = indeterminate;
  }

  public Boolean getLoading() {
    return loading;
  }

  public void setLoading(Boolean loading) {
    this.loading = loading;
  }

  public Boolean getMatched() {
    return matched;
  }

  public void setMatched(Boolean matched) {
    this.matched = matched;
  }

  public Boolean getRemoved() {
    return removed;
  }

  public void setRemoved(Boolean removed) {
    this.removed = removed;
  }

  public Boolean getRendered() {
    return rendered;
  }

  public void setRendered(Boolean rendered) {
    this.rendered = rendered;
  }

  public Boolean getSelectable() {
    return selectable;
  }

  public void setSelectable(Boolean selectable) {
    this.selectable = selectable;
  }

  public Boolean getSelected() {
    return selected;
  }

  public void setSelected(Boolean selected) {
    this.selected = selected;
  }
}
