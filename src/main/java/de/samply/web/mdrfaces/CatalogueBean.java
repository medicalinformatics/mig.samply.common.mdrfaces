/*
 * Copyright (C) 2018 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.web.mdrfaces;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Catalogue;
import de.samply.common.mdrclient.domain.Code;
import de.samply.common.mdrclient.domain.CodePredicate;
import de.samply.common.mdrclient.domain.EnumDataType;
import de.samply.common.mdrclient.domain.Root;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.jsf.MdrUtils;
import de.samply.web.mdrfaces.model.CatalogueItree;
import de.samply.web.mdrfaces.model.CatalogueNode;
import de.samply.web.mdrfaces.model.State;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "CatalogueBean")
@ApplicationScoped
public class CatalogueBean implements Serializable {
  private static Map<String, String> catalogues = new HashMap<>();

  public static Map<String, String> getCatalogues() {
    return catalogues;
  }

  public static void preloadCatalogue(
      String urn, final String language, String accessToken, String authUserId) {
    preloadCatalogue(urn, language, accessToken, authUserId, true);
  }

  public static void preloadCatalogue(
      String urn,
      final String language,
      String accessToken,
      String authUserId,
      boolean onlyValidNodesSelectable) {
    getCatalogueJson(urn, null, language, accessToken, authUserId, onlyValidNodesSelectable);
  }

  public static void preloadCatalogues(
      List<String> urns, final String language, String accessToken, String authUserId) {
    preloadCatalogues(urns, language, accessToken, authUserId, true);
  }

  /**
   * TODO: add javadoc.
   */
  public static void preloadCatalogues(
      List<String> urns,
      final String language,
      String accessToken,
      String authUserId,
      boolean onlyValidNodesSelectable) {
    for (String urn : urns) {
      preloadCatalogue(urn, language, accessToken, authUserId, onlyValidNodesSelectable);
    }
  }

  public static final String getCatalogueJson(
      final String mdrId,
      final String selectedValue,
      final String language,
      String accessToken,
      String authUserId) {
    return getCatalogueJson(mdrId, selectedValue, language, accessToken, authUserId);
  }

  /**
   * Get the (hierarchical) JSON Catalogue Data.
   *
   * @param mdrId the MDR ID of the catalogue
   * @return the select item from a MDR catalogue
   */
  public static final String getCatalogueJson(
      final String mdrId,
      final String selectedValue,
      final String language,
      String accessToken,
      String authUserId,
      boolean onlyValidNodesSelectable) {

    if (CatalogueBean.getCatalogues().containsKey(mdrId)) {
      return CatalogueBean.getCatalogues().get(mdrId);
    }

    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

    List<CatalogueNode> catalogueNodes = new ArrayList<>();

    try {
      Validations dataElementValidations =
          mdrClient.getDataElementValidations(mdrId, language, accessToken, authUserId);

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        Catalogue catalogue =
            mdrClient.getDataElementCatalogue(mdrId, language, accessToken, authUserId);

        Root root = catalogue.getRoot();

        for (String subCode : root.getSubCodes()) {
          if (subCode.equalsIgnoreCase("null")) {
            continue;
          }
          CatalogueNode cn =
              createCatalogueNodeForCode(
                  catalogue, subCode, selectedValue, onlyValidNodesSelectable);
          catalogueNodes.add(cn);
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      e.printStackTrace();
    }

    Collections.sort(catalogueNodes);

    String catalogueAsString = new Gson().toJson(catalogueNodes);
    CatalogueBean.getCatalogues().put(mdrId, catalogueAsString);

    return catalogueAsString;
  }

  public static final String getCatalogueJson(
      final String mdrId, final String language, String accessToken, String authUserId) {
    return getCatalogueJson(mdrId, null, language, accessToken, authUserId);
  }

  /** Get the filtered JSON Catalogue Data. */
  public static final String getFilteredCatalogueJson(
      final String mdrId,
      final String query,
      final String language,
      String accessToken,
      String authUserId,
      boolean onlyValidNodesSelectable) {

    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

    List<CatalogueNode> catalogueNodes = new ArrayList<>();

    try {
      Validations dataElementValidations =
          mdrClient.getDataElementValidations(mdrId, language, accessToken, authUserId);

      if (dataElementValidations.getDatatype().equals(EnumDataType.CATALOG.name())) {
        Catalogue catalogue =
            mdrClient.getFilteredDataElementCatalogue(
                mdrId, language, query, accessToken, authUserId);

        Root root = catalogue.getRoot();

        for (String subCode : root.getSubCodes()) {
          if (subCode.equalsIgnoreCase("null")) {
            continue;
          }
          CatalogueNode cn =
              createCatalogueNodeForCode(catalogue, subCode, null, onlyValidNodesSelectable);
          catalogueNodes.add(cn);
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      e.printStackTrace();
    }

    Collections.sort(catalogueNodes);

    String catalogueAsString = new Gson().toJson(catalogueNodes);
    CatalogueBean.getCatalogues().put(mdrId, catalogueAsString);

    return catalogueAsString;
  }

  /**
   * TODO: add javadoc.
   */
  public static CatalogueNode createCatalogueNodeForCode(
      Catalogue catalogue, String code, String selectedValue, boolean onlyValidNodesSelectable) {
    CatalogueNode catalogueNode = new CatalogueNode();
    com.google.common.base.Optional<Code> thisCodeOptional =
        Iterables.tryFind(catalogue.getCodes(), new CodePredicate(code));
    if (thisCodeOptional.isPresent()) {
      Code thisCode = thisCodeOptional.get();
      List<Object> subCodes = thisCode.getSubCodes();

      if (subCodes != null && subCodes.size() > 0) {
        catalogueNode.setChildren(new ArrayList<CatalogueNode>());
        for (Object subCode : subCodes) {
          Optional<Code> subCodeOptional =
              Iterables.tryFind(catalogue.getCodes(), new CodePredicate((String) subCode));
          catalogueNode
              .getChildren()
              .add(
                  createCatalogueNodeForCode(
                      catalogue,
                      subCodeOptional.get().getCode(),
                      selectedValue,
                      onlyValidNodesSelectable));
        }
        if (catalogueNode.getChildren() != null) {
          Collections.sort(catalogueNode.getChildren());
        }
      }

      catalogueNode.setCode(code);
      String designation = MdrUtils.getDesignation(thisCode.getDesignations());
      String definition = MdrUtils.getDefinition(thisCode.getDesignations());
      StringBuilder nodeText = new StringBuilder();
      if (thisCode.getIsValid()) {
        nodeText.append("[");
        nodeText.append(code);
        nodeText.append("]: ");
      }

      nodeText.append(designation);
      if (definition != null && !definition.equals(designation)) {
        nodeText.append(" (");
        nodeText.append(definition);
        nodeText.append(")");
      }

      catalogueNode.setText(nodeText.toString());
      CatalogueItree itree = new CatalogueItree();
      State state = new State();

      // if this is the selected code, set the corresponding state for the node
      //            if (selectedValue != null && !selectedValue.isEmpty() &&
      // selectedValue.equals(code)) {
      //                state.setSelected(true);
      //            }
      state.setSelectable(!onlyValidNodesSelectable || thisCode.getIsValid());
      itree.setState(state);
      catalogueNode.setItree(itree);
    }
    return catalogueNode;
  }

  @PostConstruct
  public void init() {}
}
