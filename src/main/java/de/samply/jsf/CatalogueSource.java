/*
 * Copyright (C) 2018 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.jsf;

import com.google.common.base.Charsets;
import de.samply.web.mdrfaces.CatalogueBean;
import de.samply.web.mdrfaces.DynamicCatalogueBean;
import de.samply.web.mdrfaces.MdrContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

@ManagedBean(name = "CatalogueSource")
@RequestScoped
public class CatalogueSource {

  public static final String QUERY_PARAMETER_DATAELEMENT = "de";
  public static final String QUERY_PARAMETER_NODE = "node";
  public static final String QUERY_PARAMETER_SEARCH = "query";
  public static final String QUERY_PARAMETER_ACCESSTOKEN = "access_token";
  public static final String QUERY_PARAMETER_AUTH_USERID = "auth_userid";
  private static final String CONTENT_TYPE_JSON_UTF8 = "application/json; charset=utf-8";

  /**
   * Get a map with query parameters from an URL.
   *
   * @param queryString e.g. http://www.foo.bar?p1=foo&p2=bar
   * @return a map with the query parameters. e.g. "p1"->"foo", "p2"->"bar"
   */
  public static Map<String, String> getQueryParameters(String queryString) {
    String decodedQueryString;
    try {
      decodedQueryString = URLDecoder.decode(queryString, Charsets.UTF_8.name());
    } catch (UnsupportedEncodingException e) {
      decodedQueryString = queryString;
    }

    Map<String, String> queryParameterMap = new HashMap<>();

    String[] tokens = decodedQueryString.split("[\\?&]+");

    for (String token : tokens) {
      String[] keyValueSplit = token.split("=");
      if (keyValueSplit != null && keyValueSplit.length == 2) {
        queryParameterMap.put(keyValueSplit[0], keyValueSplit[1]);
      }
    }

    return queryParameterMap;
  }

  public static String getPath() {
    return MdrContext.getMdrContext().getCatalogueSource();
  }

  /**
   * TODO: add javadoc.
   */
  public void getCatalogueNode() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
    Map<String, String> queryParameterMap = getQueryParameters(request.getQueryString());
    String language = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);

    response.setContentType(CONTENT_TYPE_JSON_UTF8);
    try {
      PrintWriter printWriter = response.getWriter();
      if (queryParameterMap.get(QUERY_PARAMETER_SEARCH) != null
          && !queryParameterMap.get(QUERY_PARAMETER_SEARCH).isEmpty()) {
        printWriter.print(getFilteredCatalogueJson(queryParameterMap, language));
      } else if (queryParameterMap.get(QUERY_PARAMETER_NODE) != null
          && !queryParameterMap.get(QUERY_PARAMETER_NODE).isEmpty()) {
        printWriter.print(getCatalogueJson(queryParameterMap, language));
      } else {
        printWriter.print(getFullCatalogueJson(queryParameterMap, language));
      }
    } catch (IOException ex) {
      throw new FacesException(ex);
    }
    context.responseComplete();
  }

  private String getCatalogueJson(Map<String, String> parameters, String language) {
    return DynamicCatalogueBean.getCatalogueJson(
        parameters.get(QUERY_PARAMETER_DATAELEMENT),
        parameters.get(QUERY_PARAMETER_NODE),
        null,
        language,
        parameters.get(QUERY_PARAMETER_ACCESSTOKEN),
        parameters.get(QUERY_PARAMETER_AUTH_USERID),
        false);
  }

  private String getFullCatalogueJson(Map<String, String> parameters, String language) {
    return CatalogueBean.getCatalogueJson(
        parameters.get(QUERY_PARAMETER_DATAELEMENT),
        null,
        language,
        parameters.get(QUERY_PARAMETER_ACCESSTOKEN),
        parameters.get(QUERY_PARAMETER_AUTH_USERID),
        false);
  }

  private String getFilteredCatalogueJson(Map<String, String> parameters, String language) {
    return CatalogueBean.getFilteredCatalogueJson(
        parameters.get(QUERY_PARAMETER_DATAELEMENT),
        parameters.get(QUERY_PARAMETER_SEARCH),
        language,
        parameters.get(QUERY_PARAMETER_ACCESSTOKEN),
        parameters.get(QUERY_PARAMETER_AUTH_USERID),
        false);
  }
}
