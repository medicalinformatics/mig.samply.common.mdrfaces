/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.jsf;

import java.io.Serializable;

/**
 * Relevant information for the user session.
 *
 * @author diogo
 */
public class LoggedUser implements Serializable {

  private static final long serialVersionUID = -7236432275860137728L;

  /** Local user ID. */
  private int id;

  /** The user name. */
  private String name;

  /** identifier of the user in the central authentications server. */
  private String authId;

  /** The user email. */
  private String email;

  /** the access token - used for REST calls authentication and to identify temporarily the user. */
  private String accessToken;

  /** Instantiate a logged user. */
  public LoggedUser() {}

  /**
   * Get the local user id.
   *
   * @return the id
   */
  public final int getId() {
    return id;
  }

  /**
   * Set the local user id.
   *
   * @param id the id to set
   */
  public final void setId(final int id) {
    this.id = id;
  }

  /**
   * Get the user name.
   *
   * @return the name
   */
  public final String getName() {
    return name;
  }

  /**
   * Set the user name.
   *
   * @param name the name to set
   */
  public final void setName(final String name) {
    this.name = name;
  }

  /**
   * Get the user's id in the central authentications server.
   *
   * @return the authId
   */
  public final String getAuthId() {
    return authId;
  }

  /**
   * Set the user's id in the central authentications server.
   *
   * @param authId the authId to set
   */
  public final void setAuthId(final String authId) {
    this.authId = authId;
  }

  /**
   * Get the user's email address.
   *
   * @return the email
   */
  public final String getEmail() {
    return email;
  }

  /**
   * Set the user's email address.
   *
   * @param email the email to set
   */
  public final void setEmail(final String email) {
    this.email = email;
  }

  /**
   * Get the access token used for REST calls authentication and to identify temporarily the user.
   *
   * @return the accessToken
   */
  public final String getAccessToken() {
    return accessToken;
  }

  /**
   * Set the access token used for REST calls authentication and to identify temporarily the user.
   *
   * @param accessToken the accessToken to set
   */
  public final void setAccessToken(final String accessToken) {
    this.accessToken = accessToken;
  }
}
