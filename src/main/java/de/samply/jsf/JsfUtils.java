/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see http://www.gnu.org/licenses.
 *
 * <p>Additional permission under GNU GPL version 3 section 7:
 *
 * <p>If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.jsf;

import java.util.Locale;
import javax.el.ELException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Frequently called methods that use JSF functionality.
 *
 * @author diogo
 */
public final class JsfUtils {

  /** User session name - it is shared between mdrfaces and projects that use this component. */
  public static final String SESSION_USER = "samply_session_user"; // $NON-NLS-1$

  /** Prevent instantiation - utility class. */
  private JsfUtils() {}

  /**
   * Extract a JSF attribute from the JSF current context. e.g. attribute defined in xhtml as
   * <composite:attribute name="mdrId" required="true" />
   *
   * @param attribute the attribute to extract e.g. mdrId
   * @return the value of the attribute
   */
  public static String getJsfAttribute(final String attribute) throws ELException {
    FacesContext fc = FacesContext.getCurrentInstance();
    String attributeValue =
        fc.getApplication()
            .evaluateExpressionGet(fc, "#{cc.attrs." + attribute + "}", String.class);
    return attributeValue;
  }

  /**
   * Get the logged in user.
   *
   * @return the logged in user
   */
  public static LoggedUser getLoggedUser() {
    // check the session
    HttpServletRequest request =
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    return (LoggedUser) request.getSession().getAttribute(SESSION_USER);
  }

  /**
   * Get the access token from the logged in user.
   *
   * @return the access token if there is a user logged in, null otherwise.
   */
  public static String getAccessToken() {
    LoggedUser loggedUser = getLoggedUser();
    return loggedUser != null ? loggedUser.getAccessToken() : null;
  }

  /**
   * Get the user authentication server ID from the logged in user.
   *
   * @return the user authentication server ID if there is a user logged in, null otherwise.
   */
  public static String getUserAuthId() {
    LoggedUser loggedUser = getLoggedUser();
    return loggedUser != null ? loggedUser.getAuthId() : null;
  }

  /**
   * Gets the locale of the JSF application.
   *
   * @return the defined locale
   */
  public static Locale getLocale() {
    if (FacesContext.getCurrentInstance() != null) {
      return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    } else {
      return new Locale("de");
    }
  }

  /**
   * Gets the locale of the JSF application.
   *
   * @return the defined locale
   */
  public static String getLocaleLanguage() {
    return getLocale().getLanguage();
  }

  /**
   * Gets the display language of the JSF application.
   *
   * @return the defined display language (e.g. "English")
   */
  public static String getDisplayLanguage() {
    return getLocale().getDisplayLanguage();
  }
}
