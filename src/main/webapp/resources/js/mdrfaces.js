$(function() {
    /* boolean indeterminate */
	$("input[data-indeterminate='true']").prop("indeterminate", true);

	/* for tooltip support */
	$('.hasTooltip').tooltip();

	/* event handler for a non-dynamic catalog*/
  $('body').on("click", ".browse-full-catalog", function() {
    if ($(this).siblings('.modal').length) {
      $(this).siblings('.modal').modal({backdrop: false});
    } else {
      $("[id='catalogue_modal_" + $(this).siblings(':input').data('mdrid') + "']").modal({backdrop: false});
    }
    $(this).siblings(':input').attr('data-activeModal', 'true');
  });

  $('body').on('show.bs.modal', '#dynamic_catalogue_modal', function(e) {
    $('.loader').show();
    var modal = $(this);
    var button = $(e.relatedTarget);
    var input = $(button).siblings('.mdrfaces-input');
    var catalogSource = input.data('catalogue_source');
    var mdrId = input.data('mdrid');

    input.attr('data-activeModal', 'true');
    modal.find('.modal-title').text(input.data('labeltext'));
    modal.find('.modal-content').data('urn', mdrId);
    modal.find('.modal-content').data('catalogue_source', catalogSource);
    initDynamicTree(catalogSource, mdrId);
  }).on('hidden.bs.modal', '#dynamic_catalogue_modal', function(e) {
    $('.catalog-search-clear').trigger("click");
  });

  $('body').on("click", ".clear-selection", function() {
    $(this).siblings('.mdrfaces-input').val('');
  });

  $('body').on("click", ".catalogue-filter", function() {
    initFilteredTree(
        $(this).parentsUntil('.modal-content').parent().data('catalogue_source'),
        $(this).parentsUntil('.modal-content').parent().data('urn'),
        $(this).parent().siblings(':input').val());
  });

  $('body').on("keyup", ".catalog-search-text", function(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      $(".catalogue-filter").click();
      return false;
    }
  });

  $('body').on("keydown keypress", ".catalog-search-text", function(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      return false;
    }
  });
});

/**
 * (Re)initialize datepickers on fields with data type date or datetime.
 * @param language Code of the language in which the datepicker should be
 *    displayed.
 */
function reinitDatepickers(language) {
    /* date picker: activate also on icon click */
    if ($('body').css('position') == 'relative') {
        $('.datepicker, .datetimepicker').find('input').datetimepicker({
            widgetParent: "body",
            locale: language
        });
        $('.datepicker, .datetimepicker').on("click focus", function() {
            $('.bootstrap-datetimepicker-widget').css({
                'top': $(this).offset().top + $(this).height(),
                'left': $(this).offset().left
            })
        })
    } else {
        $('.datepicker, .datetimepicker').find('input').datetimepicker({
          locale: language
        });
    }

    $('.datepicker').find('.input-group-addon').click(function() {
        $(this).closest('.datepicker').find('input').focus();
    });
    $('.datetimepicker').find('.input-group-addon').click(function() {
        $(this).closest('.datetimepicker').find('input').focus();
    });
}

/**
 * Convert comma to point in an input. Only the first occurrence is being
 * replaced (behaviour of String.replace), as multiple decimal separators do
 * not make sense anyway.
 * @param element Input (DOM element) which to convert.
 */
function convertDecimalSeparator(element) {
  var value = $(element).val();
  $(element).val(value.replace(',', '.'));
}