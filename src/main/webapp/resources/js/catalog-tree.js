/*
 * Initialize a tree that is dynamically loading children of nodes
 *
 * Whenever possible, use this implementation
 */
function initDynamicTree(source, urn) {
    var tree = new InspireTree({
        data: function(node) {
          return $.getJSON(source + '?de=' + urn + '&node=' + (node ? node.id : 'root'));
      }
    });

    new InspireTreeDOM(tree, {
        target: '.catalogue-tree'
    });

    tree.on('node.selected', function(event, node) {
        var inputElement =  $('*[data-activeModal="true"]');
        inputElement.val(event.code);
        inputElement.removeAttr('data-activeModal');
        $('.modal').modal('hide');
        tree.clearSearch();
        tree.deselectDeep(tree.selected());
    });

    tree.on('node.deselected', function(event, node) {
        var inputElement =  $('*[data-activeModal="true"]');
        inputElement.val('');
    });

    tree.on('model.loaded', function(event, node) {
        var sel = $('.selected').data('uid');
        if (sel != null) {
            if (tree.node(sel) != null) {
                tree.node(sel).expandParents();
            }
        }
        $('.loader').hide();
    });
}

/*
 *Initialize a tree that is instantly fully loading a filtered catalog
 */
function initFilteredTree(source, urn, query) {
  $('.loader').show();
  $('.catalogue-tree').hide();

  var filteredTree = new InspireTree({
    data: $.getJSON(source + '?de=' + urn + '&query=' + query)
  });

  new InspireTreeDOM(filteredTree, {
    target: '.catalogue-tree-filtered'
  });

  filteredTree.on('model.loaded', function(event, node) {
    $('.loader').hide();
    $('.catalogue-searchresults').show();
  });

  filteredTree.on('node.selected', function(event, node) {
    var inputElement =  $('*[data-activeModal="true"]');
    inputElement.val(event.code);
    inputElement.removeAttr('data-activeModal');
    $('.modal').modal('hide');
    filteredTree.clearSearch();
    filteredTree.deselectDeep(filteredTree.selected());
  });
}

/*
 *Initialize a tree that is instantly fully loading the whole catalog
 *
 * This variant is suitable for smaller catalogs (e.g. country lists)
 */
function initTree(elementId, urn, source) {

  elementId = elementId.replace(/\:/g,'\\:');

  var tree;
  if (source == null || source == "undefined" || source.length < 1) {
    var treeData = $('.' + elementId + '_catalogue-data').html();
    tree = new InspireTree({
      data: $.parseJSON(treeData)
    });
  } else {
    tree = new InspireTree({
      data: $.getJSON(source + '?de=' + urn)
    });
  }

  new InspireTreeDOM(tree, {
    target: '.' + elementId + '_catalogue-tree'
  });

  tree.on('node.selected', function(event, node) {
    var inputElement =  $('*[data-activeModal="true"]');
    inputElement.val(event.code);
    inputElement.removeAttr('data-activeModal');
    $('.modal').modal('hide');
    tree.clearSearch();
    tree.deselectDeep(tree.selected());
  });

  tree.on('node.deselected', function(event, node) {
    $('.' + elementId + '_catalogueSelection').val('');
  });

  tree.on('model.loaded', function(event, node) {
    var sel = $('.selected').data('uid');
    if (sel != null) {
      if (tree.node(sel) != null) {
        tree.node(sel).expandParents();
      }
    }
  });

  $('.' + elementId + '_tree-filter').on('keyup', _.debounce(function(event) {
    tree.search(this.value);
  }, 500));
}

function handleClear(data) {
  var status = data.status;
  if (status === "success") {
    $('.catalogue-searchresults').hide();
    $('.catalogue-tree').show();
  }
}